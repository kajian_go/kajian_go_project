package harmoni.kajiango.engine.APIConnection.HomeService;

import android.text.format.Time;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import harmoni.kajiango.model.Kajian;
import harmoni.kajiango.model.Mosque;
import harmoni.kajiango.model.Ustadz;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public class SubscribedService {

    public List<Kajian> loadSubscribed() {
        List<Kajian> subscribedList = new ArrayList<>();
        for(int i=0;i<10;i++){
            Kajian kajian = new Kajian();
            kajian.setIdKajian(i);
            kajian.setTitle("Welcome to kajain "+(i+1));
            kajian.setName("Mauris blandit");

            //TODO substring place until 60  char then add "..."
            if(i==2){
                kajian.setPlace("Lorem ipsum dolor sit amet ...");
            }else{
                kajian.setPlace("Lorem ipsum dolor sit amet, consectetur adipiscing elit viverra fusce...");
            }
            kajian.setDescription("Donec sollicitudin molestie malesuada. Pellentesque in ipsum id orci porta dapibus.");
            Time time = new Time();
            time.setToNow();
            kajian.setDate(new Date(time.toMillis(false)));
            //kajian.setTimeStart(new Date(time.toMillis(false)));
            //kajian.setTimeEnd(new Date(time.toMillis(false)+10000));
            kajian.setUrl("htt://www.google.com");
            kajian.setPic("this is picture");

            //set ustadz
            Ustadz ustadz = new Ustadz();
            ustadz.setName("John Doe");
            kajian.setUstadz(ustadz);

            //set mosque
            Mosque mosque = new Mosque();
            mosque.setName("Neque porro quisquam");
            kajian.setMosque(mosque);
            subscribedList.add(kajian);
        }
        return subscribedList;
    }
}
