package harmoni.kajiango.engine.APIConnection.HomeService;

import android.text.format.Time;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import harmoni.kajiango.model.Kajian;
import harmoni.kajiango.model.Mosque;
import harmoni.kajiango.model.Ustadz;
import harmoni.kajiango.utils.AppConstants;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public class NearestKajianService {

    //region public methods
    public List<Kajian> loadNearestKajianList(String jsonNearestKajian) throws JSONException, ParseException {
        List<Kajian> nearestKajianList = new ArrayList<>();

        JSONObject jsonOKajian = new JSONObject(jsonNearestKajian);
        JSONArray jsonAKajian = jsonOKajian.getJSONArray(AppConstants.JSONIKajianListing.COLLECTION);
        for (int i =0; i<jsonAKajian.length();i++){
            Kajian kajian = new Kajian();
            JSONObject aJsonOKajian = jsonAKajian.getJSONObject(i);
            kajian.setIdKajian(aJsonOKajian.getInt(AppConstants.JSONIKajianListing.ID_KAJIAN));
            kajian.setName(aJsonOKajian.getString(AppConstants.JSONIKajianListing.NAME));
            kajian.setPlace(aJsonOKajian.getString(AppConstants.JSONIKajianListing.PLACE));

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            kajian.setDate(dateFormat.parse(aJsonOKajian.getString(AppConstants.JSONIKajianListing.DATE)));

            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            Date time = timeFormat.parse(aJsonOKajian.getString(AppConstants.JSONIKajianListing.TIME_START));
            GregorianCalendar gTimeStart = new GregorianCalendar();
            gTimeStart.setTime(time);
            kajian.setTimeStart(gTimeStart);

            time = timeFormat.parse(aJsonOKajian.getString(AppConstants.JSONIKajianListing.TIME_END));
            GregorianCalendar gTimeEnd = new GregorianCalendar();
            gTimeEnd.setTime(time);
            kajian.setTimeEnd(gTimeEnd);

            kajian.setPic(aJsonOKajian.getString(AppConstants.JSONIKajianListing.PIC));
            kajian.setAttendance(aJsonOKajian.getInt(AppConstants.JSONIKajianListing.ATTENDANCE));

            Ustadz ustadz = new Ustadz();
            ustadz.setName(aJsonOKajian.getString(AppConstants.JSONIKajianListing.USTADZ));
            kajian.setUstadz(ustadz);

            Mosque mosque = new Mosque();
            mosque.setIdMosque(aJsonOKajian.getInt(AppConstants.JSONIKajianListing.ID_MOSQUE));
            mosque.setName(aJsonOKajian.getString(AppConstants.JSONIKajianListing.MOSQUE_NAME));
            mosque.setAddress(aJsonOKajian.getString(AppConstants.JSONIKajianListing.MOSQUE_ADDRESS));
            kajian.setMosque(mosque);

            nearestKajianList.add(kajian);
        }

        return nearestKajianList;
    }
    //endregion
}
