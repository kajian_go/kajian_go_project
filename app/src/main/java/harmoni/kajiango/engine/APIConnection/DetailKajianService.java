package harmoni.kajiango.engine.APIConnection;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import harmoni.kajiango.model.Kajian;
import harmoni.kajiango.model.Mosque;
import harmoni.kajiango.model.Ustadz;
import harmoni.kajiango.utils.AppConstants;

/**
 * Created by Aprilio Pajri on 21-Mar-17.
 */
public class DetailKajianService {
    //region public methods
    public Kajian loadDetailKajian(String jsonDetailKajian) throws JSONException, ParseException {
        JSONObject jsonOKajian = new JSONObject(jsonDetailKajian);
        //kajian info
        Kajian kajianDetail = new Kajian();
        kajianDetail.setIdKajian(jsonOKajian.getInt(AppConstants.JSONIKajianDetail.ID_KAJIAN));
        kajianDetail.setName(jsonOKajian.getString(AppConstants.JSONIKajianDetail.NAME));

        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
        Date timeStart = timeFormatter.parse(jsonOKajian.getString(AppConstants.JSONIKajianDetail.TIME_START));
        GregorianCalendar gTimeStart = new GregorianCalendar();
        gTimeStart.setTime(timeStart);
        kajianDetail.setTimeStart(gTimeStart);

        Date timeEnd = timeFormatter.parse(jsonOKajian.getString(AppConstants.JSONIKajianDetail.TIME_END));
        GregorianCalendar gTimeEnd = new GregorianCalendar();
        gTimeEnd.setTime(timeEnd);
        kajianDetail.setTimeEnd(gTimeEnd);

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        kajianDetail.setDate(dateFormatter.parse(jsonOKajian.getString(AppConstants.JSONIKajianDetail.DATE)));

        kajianDetail.setPlace(jsonOKajian.getString(AppConstants.JSONIKajianDetail.PLACE));
        kajianDetail.setPic(jsonOKajian.getString(AppConstants.JSONIKajianDetail.PIC));
        kajianDetail.setAttendance(jsonOKajian.getInt(AppConstants.JSONIKajianDetail.ATTEND));
        kajianDetail.setDescription(jsonOKajian.getString(AppConstants.JSONIKajianDetail.DESCRIPTION));

        //ustadz info
        Ustadz ustadz = new Ustadz();
        ustadz.setName(jsonOKajian.getString(AppConstants.JSONIKajianDetail.USTADZ));
        kajianDetail.setUstadz(ustadz);

        //mosque info
        Mosque mosque = new Mosque();
        mosque.setName(jsonOKajian.getString(AppConstants.JSONIKajianDetail.MOSQUE));
        mosque.setAddress(jsonOKajian.getString(AppConstants.JSONIKajianDetail.ADDRESS));
        mosque.setLatitude(Double.parseDouble(jsonOKajian.getString(AppConstants.JSONIKajianDetail.LATITUDE)));
        mosque.setLongitude(Double.parseDouble(jsonOKajian.getString(AppConstants.JSONIKajianDetail.LONGITUDE)));
        kajianDetail.setMosque(mosque);

        return kajianDetail;
    }
    //endregion
}
