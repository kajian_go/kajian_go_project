package harmoni.kajiango.engine.APIConnection.HomeService;

import java.util.ArrayList;
import java.util.List;

import harmoni.kajiango.model.Mosque;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public class MosqueService {
    public List<Mosque> loadMosque() {
        List<Mosque> mosqueList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Mosque mosque = new Mosque();
            mosque.setName("Nulla porttitor accumsan tincidunt.");
            mosque.setAddress("Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.");
            mosqueList.add(mosque);
        }
        return mosqueList;
    }
}
