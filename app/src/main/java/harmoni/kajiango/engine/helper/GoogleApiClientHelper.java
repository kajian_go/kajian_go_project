package harmoni.kajiango.engine.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by Aprilio Pajri on 11-Jan-17.
 */
public class GoogleApiClientHelper {
    //region attribute
    private static GoogleApiClient googleApiClient;
    private GoogleSignInOptions googleSignInOptions;
    private GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener;
    private Context context;
    private FragmentActivity fragmentActivity;
    public static GoogleSignInAccount account;

    private static final String LOG_TAG = "GoogleApiClientHelper";
    //endregion

    //region constructors
    public GoogleApiClientHelper(Context context, FragmentActivity fragmentActivity,
                                 GoogleSignInOptions googleSignInOptions, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener){
        this.context = context;
        this.fragmentActivity = fragmentActivity;
        this.googleSignInOptions = googleSignInOptions;
        this.onConnectionFailedListener = onConnectionFailedListener;
        buildGoogleApiClient();
    }

    public GoogleApiClientHelper(Context context, FragmentActivity fragmentActivity,
                                 GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener){
        this.context = context;
        this.fragmentActivity = fragmentActivity;
        this.onConnectionFailedListener = onConnectionFailedListener;
        configureGoogleSignInOptions();
        buildGoogleApiClient();
    }
    //endregion

    //region private methods
    private void configureGoogleSignInOptions(){
        googleSignInOptions = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
    }

    private void buildGoogleApiClient(){
        googleApiClient = new GoogleApiClient.Builder(context)
                .enableAutoManage(fragmentActivity, onConnectionFailedListener)
                .addApi(Auth.GOOGLE_SIGN_IN_API,googleSignInOptions)
                .build();
    }
    //endregion

    //region public methods
    public static GoogleApiClient.OnConnectionFailedListener createOnConnectedFailedListener(final Activity activity, final Bundle extra){
        GoogleApiClient.OnConnectionFailedListener listener = new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                Log.i(LOG_TAG, "connection failed  : error " + connectionResult.getErrorCode() + ","
                        + connectionResult.getErrorMessage());

                //return result tot MainActivity
                Intent dataReturn = new Intent();
                dataReturn.putExtras(extra);
                activity.setResult(activity.RESULT_OK, dataReturn);
                activity.finish();
            }
        };
        return listener;
    }

    //endregion

    //region getter setter
    public static GoogleApiClient getGoogleApiClient(){return googleApiClient;}
    //endregion
}
