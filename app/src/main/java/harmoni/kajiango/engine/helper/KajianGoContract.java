package harmoni.kajiango.engine.helper;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by Aprilio Pajri on 23-May-17.
 */
public final class KajianGoContract {

    private KajianGoContract(){};

    //region table kajian
    public static class Kajian implements BaseColumns {
        public static final String TABLE_NAME = "kajian";
        public static final String COLUMN_NAME_ID_USTADZ     ="id_ustadz";
        public static final String COLUMN_NAME_ID_MOSQUE     ="id_mosque";
        public static final String COLUMN_NAME_NAME          ="name";
        public static final String COLUMN_NAME_PLACE         ="place";
        public static final String COLUMN_NAME_DESCRIPTION   ="description";
        public static final String COLUMN_NAME_DATE          ="date";
        public static final String COLUMN_NAME_TIME_START    ="time_start";
        public static final String COLUMN_NAME_TIME_END      ="time_end";
        public static final String COLUMN_NAME_URL           ="url";
        public static final String COLUMN_NAME_PIC           ="pic";
        public static final String COLUMN_NAME_ATTACHMENT    ="attachment";
        public static final String COLUMN_NAME_ATTEND        ="attend";
        public static final String COLUMN_NAME_CREATED_DATE  ="created_date";
    }

    private static final String SQL_CREATE_TABLE_KAJIAN =
            "CREATE TABLE "+Kajian.TABLE_NAME+" ("+
                    Kajian._ID + "INTEGER PRIMARY KEY, "+
                    Kajian.COLUMN_NAME_ID_USTADZ+" INTEGER, "+
                    Kajian.COLUMN_NAME_ID_MOSQUE+" INTEGER, " +
                    Kajian.COLUMN_NAME_NAME+" TEXT, "+
                    Kajian.COLUMN_NAME_PLACE+" TEXT "+
                    Kajian.COLUMN_NAME_DESCRIPTION+" TEXT, "+
                    Kajian.COLUMN_NAME_DATE+" TEXT, "+
                    Kajian.COLUMN_NAME_TIME_START+" TEXT, "+
                    Kajian.COLUMN_NAME_TIME_END+" TEXT, "+
                    Kajian.COLUMN_NAME_URL+" TEXT, "+
                    Kajian.COLUMN_NAME_PIC+" TEXT, "+
                    Kajian.COLUMN_NAME_ATTACHMENT+" TEXT, "+
                    Kajian.COLUMN_NAME_ATTEND+" INTEGER, "+
                    Kajian.COLUMN_NAME_CREATED_DATE+" TEXT)";

    private static final String SQL_DELETE_TABLE_KAJIAN =
            "DROP TABLE IF EXIST "+Kajian.TABLE_NAME;
    //endregion

    //region private class
    public static class KajianGoDbHelper extends SQLiteOpenHelper {

        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "KajianGoCache.db";

        public KajianGoDbHelper(Context context){
            super(context,DATABASE_NAME, null, DATABASE_VERSION);
        }
        public KajianGoDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        public KajianGoDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
            super(context, name, factory, version, errorHandler);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_TABLE_KAJIAN);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(SQL_DELETE_TABLE_KAJIAN);
            onCreate(db);
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            super.onDowngrade(db, oldVersion, newVersion);
        }
    }
    //endregion

}
