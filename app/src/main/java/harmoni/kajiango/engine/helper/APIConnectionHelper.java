package harmoni.kajiango.engine.helper;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Aprilio Pajri on 04-Mar-17.
 */
public class APIConnectionHelper {
    private static APIConnectionHelper mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;
    public static String host="http://192.168.113.1/kajiango_api";

    private APIConnectionHelper(Context context){
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized APIConnectionHelper getInstance(Context context){
        if(mInstance == null){
            mInstance = new APIConnectionHelper(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue(){
        if(mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req){
        getRequestQueue().add(req);
    }
}
