package harmoni.kajiango.utils;

/**
 * Created by Aprilio Pajri on 04-Dec-16.
 */
public class AppConstants {
    public class Section{
        public static final int NEAREST_KAJIAN = 0;
        public static final int NEWEST_KAJIAN = 1;
        public static final int MOST_POPULAR_KAJIAN =2;
        public static final int SUBSCRIBED=3;
        public static final int MOSQUE=4;
    }

    public static class JSONIKajianListing {
        public static String COLLECTION = "collection";
        public static String ID_KAJIAN = "id_kajian";
        public static String ID_MOSQUE = "id_mosque";
        public static String NAME="name";
        public static String PLACE="place";
        public static String DATE = "date";
        public static String TIME_START = "time_start";
        public static String TIME_END = "time_end";
        public static String PIC = "pic";
        public static String USTADZ = "ustadz";
        public static String ATTENDANCE = "attendance";
        public static String MOSQUE_NAME = "mosque";
        public static String MOSQUE_ADDRESS = "address";
    }

    public static class JSONIKajianDetail{
        public static String ID_KAJIAN = "id_kajian";
        public static String ID_USTADZ = "id_ustadz";
        public static String NAME="name";
        public static String PLACE="place";
        public static String DESCRIPTION = "description";
        public static String DATE = "date";
        public static String ATTEND = "attend";
        public static String TIME_START = "time_start";
        public static String TIME_END = "time_end";
        public static String URL = "url";
        public static String PIC = "pic";
        public static String ATTACHMENT = "attachment";
        public static String USTADZ = "ustadz";
        public static String MOSQUE = "mosque";
        public static String ADDRESS = "address";
        public static String LATITUDE = "latitude";
        public static String LONGITUDE = "longitude";
        public static String STATUS = "status";
    }

    public static class JSONIDonationBankAccount{
        public static String COLLECTION = "collection";
        public static String ACCOUNT_NUMBER = "account_number";
        public static String BANK = "bank";
        public static String OWNER_NAME = "owner_name";
        public static String IMAGE_URL = "image_url";
        public static String STATUS = "status";
    }
}
