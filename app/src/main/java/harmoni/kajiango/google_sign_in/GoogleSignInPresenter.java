package harmoni.kajiango.google_sign_in;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import harmoni.kajiango.R;

/**
 * Created by Aprilio Pajri on 06-Jan-17.
 */
public class GoogleSignInPresenter {
    //region attributes
    private GoogleSignInView googleSignInView;
    private GoogleSignInOptions googleSignInOptions;
    private GoogleApiClient googleApiClient;
    //endregion

    //region constructors
    public GoogleSignInPresenter(GoogleSignInView googleSignInView){
        this.googleSignInView = googleSignInView;
    }
    //endregion

    //region public methods
    public void configureGoogleSignIn() {

    }

    public void buildGoogleApiClient() {

    }
    //endregion
}
