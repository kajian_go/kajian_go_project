package harmoni.kajiango.google_sign_in;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import harmoni.kajiango.R;
import harmoni.kajiango.engine.helper.GoogleApiClientHelper;

public class GoogleSignInActivity extends AppCompatActivity implements GoogleSignInView {
    //source : https://developers.google.com/identity/sign-in/android/sign-in?configured=true

    //region atributes
    private GoogleApiClientHelper googleApiClientHelper;

    private final int REQUEST_SIGN_IN = 0;
    private final String LOG_TAG = "GoogleSignInActivity";
    public static final String KEY_SIGN_IN_RESULT = "KEY_SIGN_IN_RESULT";

    //public static GoogleSignInAccount account;
    //endregion

    //region overridden methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_sign_in);

        getSupportActionBar().hide();

        Bundle bundle = new Bundle();
        bundle.putBoolean(KEY_SIGN_IN_RESULT, false);
        googleApiClientHelper = new GoogleApiClientHelper(this, this,
                GoogleApiClientHelper.createOnConnectedFailedListener(this,bundle));

//        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions
//                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();
//
//        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        googleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
//                    @Override
//                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//                        Log.i(LOG_TAG, "connection failed  : error " + connectionResult.getErrorCode() + ","
//                                + connectionResult.getErrorMessage());
//
//                        //return result tot MainActivity
//                        Intent dataReturn = new Intent();
//                        dataReturn.putExtra(KEY_SIGN_IN_RESULT, false);
//                        setResult(RESULT_OK, dataReturn);
//                        finish();
//                    }
//
//
//                })
//                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
//                .addApi(AppIndex.API).build();

        signIn();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch(requestCode){
                case REQUEST_SIGN_IN:
                    GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                    handleSignInResult(googleSignInResult);
                    break;
            }
        }
    }

    //endregion

    //region public methods

    //endregion

    //region private methods
    private void signIn(){
        if(googleApiClientHelper.getGoogleApiClient()!=null){
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClientHelper.getGoogleApiClient());
            startActivityForResult(signInIntent,REQUEST_SIGN_IN);
        }
    }

    private void handleSignInResult(GoogleSignInResult googleSignInResult){
        if(googleSignInResult.isSuccess()){
            Log.i(LOG_TAG,"sign in success");

            GoogleApiClientHelper.account = googleSignInResult.getSignInAccount();
            Intent dataReturn = new Intent();
            dataReturn.putExtra(KEY_SIGN_IN_RESULT,true);
            setResult(RESULT_OK,dataReturn);
            finish();
        }else{
            Log.i(LOG_TAG,"sign in does not success : "+googleSignInResult.getStatus().getStatusCode()
                    +", "+googleSignInResult.getStatus().getStatusMessage());

            Intent dataReturn = new Intent();
            dataReturn.putExtra(KEY_SIGN_IN_RESULT, false);
            setResult(RESULT_OK, dataReturn);
            finish();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        googleApiClientHelper.getGoogleApiClient().connect();
//        Action viewAction = Action.newAction(
//                Action.TYPE_VIEW, // TODO: choose an action type.
//                "GoogleSignIn Page", // TODO: Define a title for the content shown.
//                // TODO: If you have web page content that matches this app activity's content,
//                // make sure this auto-generated web page URL is correct.
//                // Otherwise, set the URL to null.
//                Uri.parse("http://host/path"),
//                // TODO: Make sure this auto-generated app URL is correct.
//                Uri.parse("android-app://harmoni.kajiango.google_sign_in.GoogleSignInActivity/http/host/path")
//        );
//        AppIndex.AppIndexApi.start(googleApiClientHelper.getGoogleApiClient(), viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        Action viewAction = Action.newAction(
//                Action.TYPE_VIEW, // TODO: choose an action type.
//                "GoogleSignIn Page", // TODO: Define a title for the content shown.
//                // TODO: If you have web page content that matches this app activity's content,
//                // make sure this auto-generated web page URL is correct.
//                // Otherwise, set the URL to null.
//                Uri.parse("http://host/path"),
//                // TODO: Make sure this auto-generated app URL is correct.
//                Uri.parse("android-app://.google_sign_in.GoogleSignInActivity/http/host/path")
//        );
//        AppIndex.AppIndexApi.end(googleApiClientHelper.getGoogleApiClient(), viewAction);
//        googleApiClientHelper.getGoogleApiClient().disconnect();
    }

//    private void signOut() {
//        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
//                new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(@NonNull Status status) {
//                        Toast.makeText(getApplicationContext(),"Account has been signed out",Toast.LENGTH_SHORT).show();
//                    }
//                }
//        );
//    }
    //endregion


}
