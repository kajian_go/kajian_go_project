package harmoni.kajiango.common_activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import harmoni.kajiango.R;
import harmoni.kajiango.engine.helper.APIConnectionHelper;
import uk.co.senab.photoview.PhotoViewAttacher;

public class FullScreenImageActivity extends Activity {
    //notes : to create activity as dialog, add code to AndroidManifest.xml

    //region attribtues
    public static String EXTRA_IMAGE = "EXTRA_IMAGE";
    private final String TAG = "FullScreenImageActivity";
    private String imageUrl;
    private ImageView imgFullScreen;
    PhotoViewAttacher photoViewAttacher;
    //endregion

    //region overridden methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_full_screen_image);

        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            imageUrl = extras.getString(EXTRA_IMAGE);
            imgFullScreen = (ImageView) findViewById(R.id.ImgFullScreen);
            Picasso.with(this)
                    .load(imageUrl)
                    .placeholder(R.drawable.img_200_200)
                    .error(R.drawable.img_apple_550_900)
                    .into(imgFullScreen, new Callback() {
                        @Override
                        public void onSuccess() {
                            photoViewAttacher = new PhotoViewAttacher(imgFullScreen);
                            photoViewAttacher.update();
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }else{
            Log.i(TAG,"extras is null");
        }
    }
    //endregion
}
