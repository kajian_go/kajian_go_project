package harmoni.kajiango.common_activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import harmoni.kajiango.R;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    //region attributes
    private GoogleMap mMap;
    public static String LAT_EXTRA="LAT_EXTRA";
    public static String LNG_EXTRA="LNG_EXTRA";
    public static String TITLE_EXTRA ="TITLE_EXTRA";
    private final String TAG="MapsActivity";
    //endregion

    //region activity methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //get position
        Intent extras = getIntent();
        double lat = extras.getDoubleExtra(LAT_EXTRA,Double.MIN_VALUE);
        double lng = extras.getDoubleExtra(LNG_EXTRA, Double.MIN_VALUE);
        String title = extras.getStringExtra(TITLE_EXTRA);
        if((lat!=Double.MIN_VALUE) && (lng!=Double.MIN_VALUE)){
            // Add a marker in Sydney and move the camera
            LatLng position = new LatLng(lat, lng);

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(position);
            if(title != null) markerOptions.title(title);
            mMap.addMarker(markerOptions);

            mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
            mMap.moveCamera(CameraUpdateFactory.zoomTo(15));
        }else{
            Log.e(TAG,"Lat or lng value is not valid.");
            Log.e(TAG,"Lat : "+lat+" , Lng : "+lng);

            AlertDialog.Builder errorDialogBuilder = new AlertDialog.Builder(this);
            errorDialogBuilder.setTitle("Error");
            errorDialogBuilder.setMessage("Error while loading map");
            errorDialogBuilder.setPositiveButton("OK",null);
            errorDialogBuilder.show();
        }


    }
    //endregion
}
