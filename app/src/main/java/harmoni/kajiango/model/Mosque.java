package harmoni.kajiango.model;

/**
 * Created by Aprilio Pajri on 26-Nov-16.
 */
public class Mosque {
    //region attributes
    private int idMosque;
    private String name;
    private String username;
    private String password;
    private double latitude;
    private double longitude;
    private String url;
    private String address;
    private String telp;
    private String email;
    //endregion

    //region public methods
    public int getIdMosque() {
        return idMosque;
    }

    public void setIdMosque(int idMosque) {
        this.idMosque = idMosque;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    //endregion
}
