package harmoni.kajiango.model;

/**
 * Created by Aprilio Pajri on 26-Nov-16.
 */
public class Ustadz {
    //region attributes
    private int idUstadz;
    private Mosque mosque;
    private String name;
    private String description;
    //endregion

    //region public methods
    public int getIdUstadz() {
        return idUstadz;
    }

    public void setIdUstadz(int idUstadz) {
        this.idUstadz = idUstadz;
    }

    public Mosque getMosque() {
        return mosque;
    }

    public void setMosque(Mosque mosque) {
        this.mosque = mosque;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    //endregion
}
