package harmoni.kajiango.model;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Aprilio Pajri on 26-Nov-16.
 */
public class Kajian {
    //region attributes
    private int idKajian;

    private String title;
    private String name;
    private String place;
    private String description;
    private Date date;
    private GregorianCalendar timeStart;
    private GregorianCalendar timeEnd;
    private String url;
    private String pic;
    private String attachment;
    private int attendance;

    private Ustadz ustadz;
    private Mosque mosque;
    //endregion

    //region public metods
    public int getIdKajian() {
        return idKajian;
    }

    public void setIdKajian(int idKajian) {
        this.idKajian = idKajian;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public GregorianCalendar getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(GregorianCalendar timeStart) {
        this.timeStart = timeStart;
    }

    public GregorianCalendar getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(GregorianCalendar timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public int getAttendance() {
        return attendance;
    }

    public void setAttendance(int attendance) {
        this.attendance = attendance;
    }

    public Ustadz getUstadz() {
        return ustadz;
    }

    public void setUstadz(Ustadz ustadz) {
        this.ustadz = ustadz;
    }

    public Mosque getMosque() {
        return mosque;
    }

    public void setMosque(Mosque mosque) {
        this.mosque = mosque;
    }
    //endregion
}
