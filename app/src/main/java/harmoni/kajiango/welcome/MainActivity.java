package harmoni.kajiango.welcome;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;

import harmoni.kajiango.R;
import harmoni.kajiango.engine.helper.GoogleApiClientHelper;
import harmoni.kajiango.google_sign_in.GoogleSignInActivity;
import harmoni.kajiango.home.HomeActivity;

public class MainActivity extends FragmentActivity {

    //region attributes
    private TextView txtNoLogin;

    private final int REQUEST_SIGN_IN_RESULT_INIT=0;
    private final int REQUEST_SIGN_IN_RESULT=1;
    private final String LOG_TAG = "MainActivity";
    //endregion

    //region overridden methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //perform silent sign in, if there is account already signed in, go to home
        //performSilentSignIn();
        //if account is already logged in, his line never be reached

        txtNoLogin = (TextView) findViewById(R.id.txt_no_login);
        txtNoLogin.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch(requestCode){
                case REQUEST_SIGN_IN_RESULT :
                    boolean isSignInSuccess = data.getBooleanExtra(GoogleSignInActivity.KEY_SIGN_IN_RESULT,false);
                    handleSignInResult(isSignInSuccess);
                    break;

//                case REQUEST_SIGN_IN_RESULT_INIT :
//                    boolean isSignedIn = data.getBooleanExtra(GoogleSignInActivity.KEY_SIGN_IN_RESULT,false);
//                    if(isSignedIn){
//                        goToHome();
//                    }
//                    break;

            }
        }
    }
    //endregion

    //region public methods
    public void onClick(View view){
        switch (view.getId()){
            case (R.id.btn_log_in):
                Intent signInGoogleIntent = new Intent(this, GoogleSignInActivity.class);
                startActivityForResult(signInGoogleIntent,REQUEST_SIGN_IN_RESULT);
            break;

            case (R.id.txt_no_login):
            case (R.id.btnGoToApp) :
                goToHome();
            break;
        }
    }
    //endregion

    //region private methods
    private void goToHome(){
        Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(homeIntent);
    }

    private void performSilentSignIn(){
        GoogleApiClientHelper googleApiClientHelper = new GoogleApiClientHelper(this, this,
                new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        handleSignInResult(false);
                    }
                });

        OptionalPendingResult<GoogleSignInResult> optionalPendingResult =
                Auth.GoogleSignInApi.silentSignIn(googleApiClientHelper.getGoogleApiClient());

        if(optionalPendingResult.isDone()){
            GoogleSignInResult googleSignInResult = optionalPendingResult.get();
            GoogleSignInAccount account = googleSignInResult.getSignInAccount();
            googleApiClientHelper.account = account;
            goToHome();
            finish();
        }
    }

    private void handleSignInResult(boolean isSignInSuccess){
        if(isSignInSuccess){
            GoogleApiClientHelper.getGoogleApiClient().connect();
            goToHome();
        }else{
            new AlertDialog.Builder(getApplicationContext())
                    .setTitle(getString(R.string.error_title))
                    .setMessage(getString(R.string.sign_in_msg_error))
                    .setPositiveButton(getString(R.string.btn_ok),null)
                    .show();
        }
    }
    //endregion
}
