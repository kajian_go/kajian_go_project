package harmoni.kajiango.home.mosque;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import harmoni.kajiango.R;
import harmoni.kajiango.common_activity.FullScreenImageActivity;
import harmoni.kajiango.detail_mosque.DetailMosqueActivity;
import harmoni.kajiango.model.Mosque;

/**
 * Created by Aprilio Pajri on 25-Dec-16.
 */
public class MosqueItemAdapter extends BaseAdapter {
    //region attributes
    private Activity activity;
    private List<Mosque> mosqueList;
    private static LayoutInflater layoutInflater = null;
    private  View view;
    //endregion

    //region constructors
    public MosqueItemAdapter(Activity activity, List<Mosque> mosqueLIst) {
        this.activity = activity;
        this.mosqueList = mosqueLIst;
        layoutInflater = (LayoutInflater) activity.getLayoutInflater();
    }
    //endregion

    //region overridden methods
    @Override
    public int getCount() {
        return mosqueList.size();
    }

    @Override
    public Object getItem(int position) {
        if(position < mosqueList.size())
            return mosqueList.get(position);
        else {return null;}
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        view = convertView;

        if(convertView==null) view = layoutInflater.inflate(R.layout.frghome_mosque_list_item,null);
        TextView txtName = (TextView) view.findViewById(R.id.TxtMosqueName);
        TextView txtAddress = (TextView) view.findViewById(R.id.TxtMosqueAddress);
        Button btnDetail = (Button) view.findViewById(R.id.BtnDetail);
        ImageView imgMosque = (ImageView) view.findViewById(R.id.ImgMosque);

        Mosque mosque =(Mosque) getItem(position);
        txtName.setText(mosque.getName());
        txtAddress.setText(mosque.getAddress());
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mosqueIntent = new Intent(view.getContext(), DetailMosqueActivity.class);
                view.getContext().startActivity(mosqueIntent);
            }
        });
        imgMosque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*START show dialog fullscreen image*/
                Intent imageFullScreenIntent = new Intent(activity.getApplicationContext(), FullScreenImageActivity.class);
                imageFullScreenIntent.putExtra(FullScreenImageActivity.EXTRA_IMAGE,R.drawable.img_cat_640_480);
                activity.startActivity(imageFullScreenIntent);
                /*END show dialog fullscreen image*/
            }
        });
        return view;
    }
    //endregion

    //region public methods
    public void updateData(List<Mosque> otherMosqueList){
        this.mosqueList.addAll(otherMosqueList);
        this.notifyDataSetChanged();
    }
    //endregion
}
