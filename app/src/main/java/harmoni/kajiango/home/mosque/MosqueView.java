package harmoni.kajiango.home.mosque;

import java.util.List;

import harmoni.kajiango.model.Mosque;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public interface MosqueView {

    public void showMosque(List<Mosque> mosqueList);
    public void updateMosqueList(List<Mosque> anotherMosqueList);
}
