package harmoni.kajiango.home.mosque;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import java.util.List;

import harmoni.kajiango.R;
import harmoni.kajiango.model.Mosque;

/**
 * Created by Aprilio Pajri on 25-Dec-16.
 */
public class MosqueListingFragment extends ListFragment implements MosqueView{

    //region attributes
    private List<Mosque> mosqueList;
    private MosquePresenter mosquePresenter;
    private MosqueItemAdapter mosqueItemAdapter;
    private final String LOG_TAG = "MosqueListing";
    //endregion

    //region contsructor
    public MosqueListingFragment(){
        mosquePresenter = new MosquePresenter(this);
        mosquePresenter.loadMosque();
    }
    //endregion

    //region overriden method

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frghome_mosque_listing,container, false);
        mosqueItemAdapter = new MosqueItemAdapter(getActivity(), mosqueList);
        setListAdapter(mosqueItemAdapter);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        handleScrolledToLastItem();
    }

    //endregion

    //region implemented method from HosqueView
    @Override
    public void showMosque(List<Mosque> mosqueList) {
        this.mosqueList = mosqueList;
    }

    @Override
    public void updateMosqueList(List<Mosque> anotherMosqueList) {
        mosqueItemAdapter.updateData(anotherMosqueList);
    }

    //endregion

    //region private methods
    private void handleScrolledToLastItem(){
        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.d(LOG_TAG,"getListView().getLastVisiblePosition() : "+getListView().getLastVisiblePosition());
                Log.d(LOG_TAG,"totalItemCount : "+totalItemCount);
                if(getListView().getLastVisiblePosition() + 1 == totalItemCount){
                    mosquePresenter.updateMosqueList();
                }
            }
        });
    }
    //endregion

}
