package harmoni.kajiango.home.mosque;

import java.util.List;

import harmoni.kajiango.engine.APIConnection.HomeService.MosqueService;
import harmoni.kajiango.model.Mosque;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public class MosquePresenter {
    MosqueView mosqueView;

    //region constructor
    public MosquePresenter(MosqueView mosqueView) {
        this.mosqueView = mosqueView;
    }
    //endregion

    //region public methods
    public void loadMosque(){
        MosqueService mosqueService = new MosqueService();
        List<Mosque> mosqueList = mosqueService.loadMosque();
        mosqueView.showMosque(mosqueList);
    }

    public void updateMosqueList(){
        MosqueService mosqueService = new MosqueService();
        List<Mosque> anotherMosqueList = mosqueService.loadMosque();
        mosqueView.updateMosqueList(anotherMosqueList);
    }
    //endregion
}
