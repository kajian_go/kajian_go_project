package harmoni.kajiango.home;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import harmoni.kajiango.R;
import harmoni.kajiango.about_us.AboutUsActivity;
import harmoni.kajiango.donasi.DonationActivity;
import harmoni.kajiango.engine.helper.GoogleApiClientHelper;
import harmoni.kajiango.home.nearest_kajian.NearestKajianListingFragment;
import harmoni.kajiango.home.newest_kajian.NewestKajianListingFragment;

public class HomeActivity extends AppCompatActivity{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private HomeSectionsPagerAdapter homeSectionPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager viewPager;

    //region overridden methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

//        if(GoogleApiClientHelper.account != null){
//            toolbar.setSubtitle(String.format(getString(R.string.home_greeting),
//                    GoogleApiClientHelper.account.getDisplayName()));
//
//        }
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        homeSectionPagerAdapter = new HomeSectionsPagerAdapter(getSupportFragmentManager(), this);


        // Set up the ViewPager with the sections adapter.
        viewPager = (ViewPager) findViewById(R.id.container);

        if (viewPager != null) {
            viewPager.setAdapter(homeSectionPagerAdapter);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    switch(position){
                        case 0 :
                            NearestKajianListingFragment l = (NearestKajianListingFragment) homeSectionPagerAdapter.getItem(position);
                            l.requestData();
                            break;
                        case 1 :
                            NewestKajianListingFragment n = (NewestKajianListingFragment) homeSectionPagerAdapter.getItem(position);
                            n.requestData();
                            break;
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
            if (tabLayout != null) {
                tabLayout.setupWithViewPager(viewPager);
            }
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
//        if(GoogleApiClientHelper.account!=null){
//            menu.findItem(R.id.MItemSignIn).setVisible(false);
//            menu.findItem(R.id.MItemSignOut).setVisible(true);
//        }else{
//            menu.findItem(R.id.MItemSignIn).setVisible(true);
//            menu.findItem(R.id.MItemSignOut).setVisible(false);
//        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Intent intent;
        switch(id){
//            case R.id.MItemSignIn:
//                return true;
//            case R.id.MItemSignOut:
//                Auth.GoogleSignInApi.signOut(GoogleApiClientHelper.getGoogleApiClient()).setResultCallback(
//                    new ResultCallback<Status>() {
//                        @Override
//                        public void onResult(@NonNull Status status) {
//                            Toast.makeText(getApplicationContext(),"Account has been signed out",Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                );
//
//                intent = new Intent(getApplicationContext(), MainActivity.class);
//                startActivity(intent);
//                finish();
//                return true;
            case R.id.MItemAboutUs:
                intent = new Intent(getApplicationContext(), AboutUsActivity.class);
                startActivity(intent);
                return true;
            case R.id.MItemDonation:
                intent = new Intent(getApplicationContext(), DonationActivity.class);
                startActivity(intent);
                return true;
//            case R.id.MItemSettings:
//                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //endregion
}
