package harmoni.kajiango.home.newest_kajian;

import org.json.JSONException;

import java.text.ParseException;
import java.util.List;

import harmoni.kajiango.engine.APIConnection.HomeService.NewestKajianService;
import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public class NewestKajianPresenter {
    //region attributes
    private NewestKajianView newestKajianView;
    private NewestKajianService newestKajianService;
    //endregion

    //region constructor
    public NewestKajianPresenter(NewestKajianView newestKajianView){
        this.newestKajianView = newestKajianView;
        newestKajianService = new NewestKajianService();
    }
    //endregion

    //region public methods
    public void loadNewestKajian(String jsonNewestKajian, boolean isLoadMore) throws JSONException, ParseException {
        List<Kajian> newestKajianList = newestKajianService.loadNewestKajian(jsonNewestKajian);
        if(isLoadMore){
            newestKajianView.showMoreNewestKajian(newestKajianList);
        }else{
            newestKajianView.showNewestKajian(newestKajianList);
        }
    }
    //endregion
}
