package harmoni.kajiango.home.newest_kajian;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.List;

import harmoni.kajiango.R;
import harmoni.kajiango.engine.helper.APIConnectionHelper;
import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 04-Dec-16.
 */
public class NewestKajianListingFragment extends ListFragment implements NewestKajianView{
    //region attributes
    private List<Kajian> newestKajianList;
    private NewestKajianPresenter newestKajianPresenter;
    private NewestKajianItemAdapter adapter;
    private final String TAG = "NewestKajianListing";
    private ProgressDialog pdlgLoadNewestKajian;
    private boolean isUserScrolling;
    private View vFooterLoading;
    private LoadMoreKajianTask loadMoreKajianTask;
    //endregion

    //region constructors
    public NewestKajianListingFragment() {
        newestKajianPresenter = new NewestKajianPresenter(this);
    }
    //endregion
    //region overridden methods
    @Override
    public void onStart() {
        super.onStart();
        pdlgLoadNewestKajian = new ProgressDialog(getContext());
        pdlgLoadNewestKajian.setMessage(getString(R.string.info_message_loading_data));
        pdlgLoadNewestKajian.setCanceledOnTouchOutside(false);
        pdlgLoadNewestKajian.setIndeterminate(true);
        pdlgLoadNewestKajian.show();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LayoutInflater layoutInflater = inflater;
        ViewGroup viewGroup = container;
        View rootView = layoutInflater.inflate(R.layout.frghome_kajian_listing, viewGroup, false);

        requestNewestKajian(false,"/-1/-1/false");
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        handleScrolledToLastItem();
    }

    //endregion

    //region implemented methods from NewestKajianView
    @Override
    public void showNewestKajian(List<Kajian> newestKajianList) {
        closeDlgProgLoadNearestKajian();
        this.newestKajianList = newestKajianList;
    }

    @Override
    public void showMoreNewestKajian(List<Kajian> newestKajianList) {
        removeFooterViewLoading();
        adapter.updateData(newestKajianList);
    }

    //endregion

    //region private methods
    public void requestData(){
        Log.d(TAG,"Requesting data in NewestKajianListingFragment ...");
    }
    private void closeDlgProgLoadNearestKajian(){
        //close prgress dialog
        if(pdlgLoadNewestKajian != null)
            if(pdlgLoadNewestKajian.isShowing())
                pdlgLoadNewestKajian.dismiss();
    }

    private void handleScrolledToLastItem(){
        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                isUserScrolling = scrollState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL;
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (getListView().getLastVisiblePosition() + 1 == (totalItemCount - 1)
                        && isUserScrolling) {
                    //list view scrolled to the bottom

                    isUserScrolling = false;
                    Log.d(TAG,"load more : true");

                    if(loadMoreKajianTask == null){
                        loadMoreKajianTask = new NewestKajianListingFragment.LoadMoreKajianTask();
                        loadMoreKajianTask.execute();
                    }else{
                        if(loadMoreKajianTask.getStatus() != AsyncTask.Status.RUNNING){
                            loadMoreKajianTask = new NewestKajianListingFragment.LoadMoreKajianTask();
                            loadMoreKajianTask.execute();
                        }
                    }
                }
            }
        });
    }

    private void requestNewestKajian(final boolean isLoadMore, String params){
        Log.d(TAG,"in requestNewestKajian");

        //api url
        String url = APIConnectionHelper.host + "/kajian/newestKajianList" +
                params;
        Log.d(TAG,"url : "+url);
        //request nearest kajian

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonOResponse = new JSONObject(response.toString());

                            if (!jsonOResponse.isNull("collection")) {
                                if(isLoadMore){
                                    newestKajianPresenter.loadNewestKajian(response.toString(),isLoadMore);
                                }else{
                                    newestKajianPresenter.loadNewestKajian(response.toString(),isLoadMore);
                                    adapter = new NewestKajianItemAdapter(getActivity(), newestKajianList);
                                    setListAdapter(adapter);
                                }

                            } else {
                                displayErrorDialog(getString(R.string.error_message_loading_data));
                                Log.e(TAG, "collection is null");
                            }
                        } catch (JSONException | ParseException e) {
                            e.printStackTrace();
                            Log.e(TAG, "Error while retrieving json newest kajian : " + e.getMessage());
                            displayErrorDialog("Error while loading data");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Error on request data (onErrorResponse method) : " + error.getMessage());
                        error.printStackTrace();
                        displayErrorDialog(getString(R.string.error_message_loading_data));
                    }
                }
        );

        APIConnectionHelper.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    private void displayErrorDialog(String message) {
        closeDlgProgLoadNearestKajian();
        removeFooterViewLoading();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getString(R.string.error_title));
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(getString(R.string.btn_ok), null);
        alertDialogBuilder.show();
    }

    private void removeFooterViewLoading() {
        if(vFooterLoading != null){
            getListView().removeFooterView(vFooterLoading);
        }
    }
    //endregion

    private class LoadMoreKajianTask extends AsyncTask<Void, Integer, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            List<Kajian> nearestKajianList = adapter.getNewestKajianList();
            Kajian lastITem = nearestKajianList.get(nearestKajianList.size()-1);
            int idKajian = lastITem.getIdKajian();
            int idMosque = lastITem.getMosque().getIdMosque();
            Log.d(TAG,"id kajian : "+idKajian+"; id mosque : "+idMosque);
            requestNewestKajian(true,"/"+idKajian+"/"+idMosque);
            return null;
        }

        @Override
        protected void onPreExecute() {
            vFooterLoading = ((LayoutInflater)getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.layout_footer_loading,null,false);

            if(getListView().getFooterViewsCount() == 0){
                getListView().addFooterView(vFooterLoading);
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            removeFooterViewLoading();
        }
    }
}
