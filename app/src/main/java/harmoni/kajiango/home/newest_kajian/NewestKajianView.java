package harmoni.kajiango.home.newest_kajian;

import java.util.List;

import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public interface NewestKajianView {

    void showNewestKajian(List<Kajian> newestKajianList);
    void showMoreNewestKajian(List<Kajian> newestKajianList);
}
