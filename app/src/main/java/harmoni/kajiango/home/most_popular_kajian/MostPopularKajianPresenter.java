package harmoni.kajiango.home.most_popular_kajian;

import java.util.List;

import harmoni.kajiango.engine.APIConnection.HomeService.MostPopularKajianService;
import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public class MostPopularKajianPresenter {
    private MostPopularKajianView mostPopularKajianView;

    //region constructor
    public MostPopularKajianPresenter(MostPopularKajianView mostPopularKajianView){
        this.mostPopularKajianView = mostPopularKajianView;
    }
    //endregion

    //region public methods
    public void loadMostPopularKajian() {
        MostPopularKajianService mostPopularKajianService = new MostPopularKajianService();
        List<Kajian> mostPopularKajianList = mostPopularKajianService.loadMostPopularKajian();
        mostPopularKajianView.showMostPopularKajian(mostPopularKajianList);
    }

    public void updateMostPopularKajianList() {
        MostPopularKajianService mostPopularKajianService = new MostPopularKajianService();
        List<Kajian> mostPopularKajianList = mostPopularKajianService.loadMostPopularKajian();
        mostPopularKajianView.updateMostPopularKajianList(mostPopularKajianList);
    }
    //endregion
}
