package harmoni.kajiango.home.most_popular_kajian;

import java.util.List;

import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public interface MostPopularKajianView {
    public void showMostPopularKajian(List<Kajian> mostPopularKajianList);
    public void updateMostPopularKajianList(List<Kajian> anotherMostPopularKajianList);
}
