package harmoni.kajiango.home.most_popular_kajian;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import java.util.List;

import harmoni.kajiango.R;
import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 04-Dec-16.
 */
public class MostPopularKajianListingFragment extends ListFragment implements MostPopularKajianView{
    //region attributes
    private List<Kajian> mostPopularKajianList;
    MostPopularKajianPresenter mostPopularKajianPresenter;
    MostPopularKajianItemAdapter adapter;
    private final String LOG_TAG = "MostPopularKajian";
    //endregion

    //region overridden methods
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frghome_kajian_listing, container, false);
        mostPopularKajianPresenter = new MostPopularKajianPresenter(this);
        mostPopularKajianPresenter.loadMostPopularKajian();
        adapter = new MostPopularKajianItemAdapter(getActivity(), mostPopularKajianList);
        setListAdapter(adapter);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        handleScrolledToLastItem();
    }

    //endregion

    //region implemented method from MostPopularKajianView
    @Override
    public void showMostPopularKajian(List<Kajian> mostPopularKajianList) {
        this.mostPopularKajianList = mostPopularKajianList;

    }

    @Override
    public void updateMostPopularKajianList(List<Kajian> anotherMostPopularKajianList) {
        adapter.updateData(anotherMostPopularKajianList);
    }
    //endregion

    //region private methods
    private void handleScrolledToLastItem(){
        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.d(LOG_TAG,"getListView().getLastVisiblePosition() : "+getListView().getLastVisiblePosition());
                Log.d(LOG_TAG,"totalItemCount : "+totalItemCount);
                if(getListView().getLastVisiblePosition() + 1 == totalItemCount){
                    mostPopularKajianPresenter.updateMostPopularKajianList();
                }
            }
        });
    }
    //endregion
}
