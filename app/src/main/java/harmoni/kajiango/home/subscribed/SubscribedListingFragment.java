package harmoni.kajiango.home.subscribed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import java.util.List;

import harmoni.kajiango.R;
import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 04-Dec-16.
 */
public class SubscribedListingFragment extends ListFragment implements SubscribedView {

    //region attributes
    private List<Kajian> subscribedList;
    private SubscribedPresenter subscribedPresenter;
    private SubscribedItemAdapter adapter;
    private static final String LOG_TAG = "SubscribedListing";
    //endregion

    //region overridden methods
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frghome_kajian_listing, container, false);
        subscribedPresenter = new SubscribedPresenter(this);
        subscribedPresenter.loadSubscribed();
        adapter = new SubscribedItemAdapter(getActivity(), subscribedList);
        setListAdapter(adapter);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        handleScrolledToLastItem();
    }

    //endregion

    //endregion implemented methods from SubscribedView
    @Override
    public void showSubscribed(List<Kajian> subscribedList) {
        this.subscribedList = subscribedList;
    }

    @Override
    public void updateSubscribedList(List<Kajian> anotherSubscribedList) {
        adapter.updateData(anotherSubscribedList);
    }

    //endregion

    //region private methods
    private void handleScrolledToLastItem(){
        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.d(LOG_TAG,"getListView().getLastVisiblePosition() : "+getListView().getLastVisiblePosition());
                Log.d(LOG_TAG,"totalItemCount : "+totalItemCount);
                if(getListView().getLastVisiblePosition() + 1 == totalItemCount){
                    subscribedPresenter.updateSubscribedList();
                }
            }
        });
    }
    //endregion
}
