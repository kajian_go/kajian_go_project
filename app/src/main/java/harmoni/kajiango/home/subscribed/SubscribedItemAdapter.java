package harmoni.kajiango.home.subscribed;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import harmoni.kajiango.R;
import harmoni.kajiango.common_activity.FullScreenImageActivity;
import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 04-Dec-16.
 */
public class SubscribedItemAdapter extends BaseAdapter{

    //region attributes
    private Activity activity;
    private List<Kajian> subscribedList;
    private static LayoutInflater layoutInflater = null;
    //endregion

    //region constructors
    public SubscribedItemAdapter(Activity activity, List<Kajian> subscribedList) {
        this.activity = activity;
        this.subscribedList = subscribedList;
        layoutInflater = activity.getLayoutInflater();
    }
    //endregion

    //region overridden methods
    @Override
    public int getCount() {
        return subscribedList.size();
    }

    @Override
    public java.lang.Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(convertView == null) view = layoutInflater.inflate(R.layout.frghome_kajian_list_item,null);

        TextView txtTitle = (TextView) view.findViewById(R.id.TxtKajianTitle);
        TextView txtDate = (TextView) view.findViewById(R.id.TxtDate);
        TextView txtTime = (TextView) view.findViewById(R.id.TxtTime);
        TextView txtAddress = (TextView) view.findViewById(R.id.TxtAddress);
        TextView txtSpeaker = (TextView) view.findViewById(R.id.TxtSpeaker);
        TextView txtAttend = (TextView) view.findViewById(R.id.TxtAttend);
        Button btnDetail = (Button) view.findViewById(R.id.BtnDetail);
        ImageView imgPosterThumb = (ImageView) view.findViewById(R.id.ImgPosterThumb);
        //TODO chahange set text with string formatter in string.xml
        //example : <string name="str">%d attend</string>
        Kajian kajian = subscribedList.get(position);
        txtTitle.setText(kajian.getTitle());
        txtDate.setText("Date : "+kajian.getDate().toString());
        txtTime.setText("Time : "+kajian.getTimeStart().toString()+" to "+kajian.getTimeEnd().toString());
        txtAddress.setText("Address : "+kajian.getPlace());
        txtSpeaker.setText("Speaker : "+kajian.getUstadz().getName());
        txtAttend.setText(activity.getString(R.string.kajian_listing_attend));
        imgPosterThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*START show dialog fullscreen image*/
                Intent imageFullScreenIntent = new Intent(activity.getApplicationContext(), FullScreenImageActivity.class);
                imageFullScreenIntent.putExtra(FullScreenImageActivity.EXTRA_IMAGE,R.drawable.img_food_480_800);
                activity.startActivity(imageFullScreenIntent);
                /*END show dialog fullscreen image*/
            }
        });
        return view;
    }
    //endregion

    //region private methods
    public void updateData(List<Kajian> anotherSubscribedList){
        this.subscribedList.addAll(anotherSubscribedList);
        notifyDataSetChanged();
    }
    //endregion
}
