package harmoni.kajiango.home.subscribed;

import java.util.List;

import harmoni.kajiango.engine.APIConnection.HomeService.SubscribedService;
import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public class SubscribedPresenter {
    SubscribedView subscribedView;

    //region constructor
    public SubscribedPresenter(SubscribedView subscribedView) {
        this.subscribedView = subscribedView;
    }
    //endregion

    //region public methods
    public void loadSubscribed() {
        SubscribedService subscribedService = new SubscribedService();
        List<Kajian> subscribedList = subscribedService.loadSubscribed();
        subscribedView.showSubscribed(subscribedList);
    }

    public void updateSubscribedList() {
        SubscribedService subscribedService = new SubscribedService();
        List<Kajian> subscribedList = subscribedService.loadSubscribed();
        subscribedView.updateSubscribedList(subscribedList);
    }
    //endregion
}
