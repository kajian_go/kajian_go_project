package harmoni.kajiango.home.subscribed;

import java.util.List;

import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public interface SubscribedView {
    public void showSubscribed(List<Kajian> subscribedList);
    public void updateSubscribedList(List<Kajian> anotherSubscribedList);
}
