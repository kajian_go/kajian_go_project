package harmoni.kajiango.home;

/**
 * Created by Aprilio Pajri on 02-Dec-16.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.List;

import harmoni.kajiango.R;
import harmoni.kajiango.home.mosque.MosqueListingFragment;
import harmoni.kajiango.home.most_popular_kajian.MostPopularKajianListingFragment;
import harmoni.kajiango.home.nearest_kajian.NearestKajianListingFragment;
import harmoni.kajiango.home.newest_kajian.NewestKajianListingFragment;
import harmoni.kajiango.home.subscribed.SubscribedListingFragment;
import harmoni.kajiango.utils.AppConstants;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class HomeSectionsPagerAdapter extends FragmentPagerAdapter {


    private List<Object> kajianList;
    private Context context ;
    private NearestKajianListingFragment nearestKajianListingFragment;
    private NewestKajianListingFragment newestKajianListingFragment;
    private final String TAG="HomeSectionPagerAdapter";

    public HomeSectionsPagerAdapter(FragmentManager fm, Context context){
        super(fm);
        this.context = context;

    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a NearestKajianListingFragment (defined as a static inner class below).
        Log.d(TAG,"position : "+position);
        switch(position){
            case AppConstants.Section.NEAREST_KAJIAN:
                if(nearestKajianListingFragment==null)
                    nearestKajianListingFragment = new NearestKajianListingFragment();
                return nearestKajianListingFragment;
            case AppConstants.Section.NEWEST_KAJIAN:
                if(newestKajianListingFragment == null)
                    newestKajianListingFragment = new NewestKajianListingFragment();
                return newestKajianListingFragment;
//            case AppConstants.Section.MOST_POPULAR_KAJIAN :
//                MostPopularKajianListingFragment mostPopularKajianListingFragment = new MostPopularKajianListingFragment();
//                return mostPopularKajianListingFragment;
//            case AppConstants.Section.SUBSCRIBED:
//                SubscribedListingFragment subscribedListingFragment = new SubscribedListingFragment();
//                return subscribedListingFragment;
//            case AppConstants.Section.MOSQUE:
//                MosqueListingFragment mosqueListingFragment = new MosqueListingFragment();
//                return mosqueListingFragment;
            default :
                Log.e(TAG,"item not found");
                return null;
        }

    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case AppConstants.Section.NEAREST_KAJIAN:
                return context.getString(R.string.module_nearest);
            case AppConstants.Section.NEWEST_KAJIAN:
                return context.getString(R.string.module_newest);
//            case AppConstants.Section.MOST_POPULAR_KAJIAN:
//                return context.getString(R.string.module_most_popular);
//            case AppConstants.Section.SUBSCRIBED:
//                return context.getString(R.string.module_subscribed);
//            case AppConstants.Section.MOSQUE:
//                return context.getString(R.string.module_mosque);
            default:
                Log.e(TAG,"page title not found");
                return null;
        }
    }



    //region Getter And Setter
    //endregion
}
