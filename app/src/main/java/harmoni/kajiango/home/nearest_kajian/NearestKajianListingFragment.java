package harmoni.kajiango.home.nearest_kajian;

/**
 * Created by Aprilio Pajri on 02-Dec-16.
 */

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import harmoni.kajiango.R;
import harmoni.kajiango.engine.helper.APIConnectionHelper;
import harmoni.kajiango.engine.helper.KajianGoContract;
import harmoni.kajiango.model.Kajian;

/**
 * A placeholder fragment containing a simple view.
 */
public class NearestKajianListingFragment extends ListFragment implements NearestKajianView,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    /**
     * TODO add loading when load more data
     * TODO investigate why sometime got timeout error volley when first time loading data
     */

    //region attributes
    private final String TAG = "NearestKajianListing";
    private final int REQUEST_LOCATION_SETTINGS=1;
    private List<Kajian> nearestKajianList;
    private NearestKajianPresenter nearestKajianPresenter;
    private NearestKajianItemAdapter kajianElementAdapter;
    private boolean isUserScrolling = false;
    private static GoogleApiClient googleApiClient;
    private Location lastLocation;
    private ProgressDialog dlgProgLoadNearestKajian;
    private View vFooterLoading;
    private KajianGoContract.KajianGoDbHelper dbHelper;
    private LoadMoreKajianTask loadMoreKajianTask;
    //endregion

    //region constructors
    public NearestKajianListingFragment() {
        nearestKajianPresenter = new NearestKajianPresenter(this);
    }
    //endregion

    //region overridden methods
    @Override
    public void onStart() {
        super.onStart();
        //create dialog progress bar
        dlgProgLoadNearestKajian = new ProgressDialog(getContext());
        dlgProgLoadNearestKajian.setMessage(getString(R.string.info_message_loading_data));
        dlgProgLoadNearestKajian.setCanceledOnTouchOutside(false);
        dlgProgLoadNearestKajian.setIndeterminate(true);
        dlgProgLoadNearestKajian.show();

        dbHelper = new KajianGoContract.KajianGoDbHelper(getContext());
        initGoogleApiClient();
        if(googleApiClient != null)
            if(!googleApiClient.isConnected())
                googleApiClient.connect();
    }

    @Override
    public void onStop() {
        googleApiClient.disconnect();
        if(googleApiClient!=null) googleApiClient = null;
        if(loadMoreKajianTask!=null){
            loadMoreKajianTask.cancel(true);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");
        Log.d(TAG,"List item count : "+getListView().getCount());
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LayoutInflater layoutInflater = inflater;
        ViewGroup viewGroup = container;
        View rootView = layoutInflater.inflate(R.layout.frghome_kajian_listing, viewGroup, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        handleScrolledToLastItem();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case(REQUEST_LOCATION_SETTINGS):
                switch (resultCode){
                    case (Activity.RESULT_OK):
                        requestNearestKajian(false, null);
                        break;
                    case(Activity.RESULT_CANCELED):
                        //TODO handle user cancel enable location service
                        lastLocation = null;
                        closeDlgProgLoadNearestKajian();
                        break;
                }
                break;
        }
    }

    //endregion

    //region implemented methods from NearestKajianView
    @Override
    public void showNearestKajian(List<Kajian> nearestKajianList) {
        closeDlgProgLoadNearestKajian();
        this.nearestKajianList = nearestKajianList;
    }

    @Override
    public void showMoreNearestKajian(List<Kajian> nearestKajianList) {
        removeFooterViewLoading();
        kajianElementAdapter.updateData(nearestKajianList);
    }

    //endregion

    //region private methods
    public void requestData(){
        Log.d(TAG,"Requesting data in NearestKajianListingFragment ...");
    }
    private void initGoogleApiClient() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    private void handleScrolledToLastItem() {
        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                isUserScrolling = scrollState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL;
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (getListView().getLastVisiblePosition() + 1 == (totalItemCount - 1)
                        && isUserScrolling) {
                    //list view scrolled to the bottom


                    isUserScrolling = false;
                    Log.d(TAG,"load more : true");
                    //request data
                    if(loadMoreKajianTask == null){
                        loadMoreKajianTask = new LoadMoreKajianTask();
                        loadMoreKajianTask.execute();
                    }else{
                        if(loadMoreKajianTask.getStatus() != AsyncTask.Status.RUNNING){
                            loadMoreKajianTask = new LoadMoreKajianTask();
                            loadMoreKajianTask.execute();
                        }
                    }
                }
            }
        });
    }

    private void displayErrorDialog(String message) {
        closeDlgProgLoadNearestKajian();
        removeFooterViewLoading();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getString(R.string.error_title));
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(getString(R.string.btn_ok), null);
        alertDialogBuilder.show();
    }

    private void removeFooterViewLoading(){
        if(vFooterLoading != null){
            getListView().removeFooterView(vFooterLoading);
        }
    }

    private boolean isLocationServiceEnabled(){
        //source : http://www.developersite.org/101-176153-android
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            int locationMode;
            try {
                locationMode = Settings.Secure.getInt(getActivity().getContentResolver(),
                        Settings.Secure.LOCATION_MODE);

                return locationMode != Settings.Secure.LOCATION_MODE_OFF;
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                Log.e(TAG,"Error on isLocationServiceEnabled : "+e.getMessage());
                Log.e(TAG,e.getStackTrace().toString());
                return false;
            }
        }else{
            //TODO test on Build Version < KITKAT
            //TODO use method that is not deprecated
            String locationProviders="";
            locationProviders = Settings.Secure.getString(getActivity().getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !locationProviders.isEmpty();
        }
    }

    private void retrieveLastLocation(){
        //permission check to location service
        if (ActivityCompat.checkSelfPermission(getActivity(),
                //i've added permission to manifest so i dont give
                //handler for this permission request
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }
        Calendar calBefore = Calendar.getInstance();
        long timeRange = 0;

        do{
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            Calendar calAfter = Calendar.getInstance();
            timeRange = (calAfter.getTimeInMillis() - calBefore.getTimeInMillis())/1000;
        }while((timeRange <= 5) || (lastLocation == null));

        Log.d(TAG,"last location : "+lastLocation.getLatitude()+","+lastLocation.getLongitude());

    }

    private boolean isLocationSame(){
        //retrieve stored location
        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        String storedLocation = sharedPreferences.getString("LOCATION",null);

        if(storedLocation != null){
            String[] coordinate = storedLocation.split(";");
            double storedLatitude = Double.valueOf(coordinate[0]);
            double storedLongitude = Double.valueOf(coordinate[1]);

            float[] results = new float[1];
            Location.distanceBetween(lastLocation.getLatitude(),lastLocation.getLongitude(),storedLatitude,storedLongitude,results);
            if(results[0] < 10.0){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

    private void processRequestingNearestKajian(){
        Log.d(TAG,"in processRequestingNearestKajian");

        //check whether location service is enabled or not
        if(!isLocationServiceEnabled()){
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(3600000); //set interval for 1 hour
            //TODO turn of on pause then turn on again on resume

            LocationSettingsRequest.Builder locationSettingsRequestBuilder =
                    new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            locationSettingsRequestBuilder.setAlwaysShow(true);
            PendingResult<LocationSettingsResult> locationResult =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient,
                            locationSettingsRequestBuilder.build());

            locationResult.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                @Override
                public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                    switch(locationSettingsResult.getStatus().getStatusCode()){
                        case LocationSettingsStatusCodes.SUCCESS:
                            requestNearestKajian(false, null);
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                startIntentSenderForResult(locationSettingsResult.getStatus()
                                                .getResolution().getIntentSender(),REQUEST_LOCATION_SETTINGS,
                                         null,0,0,0,null);
                            } catch (IntentSender.SendIntentException e) {
                                e.printStackTrace();
                                displayErrorDialog(getString(R.string.error_message_request_location_settings));
                                Log.e(TAG,"Error onResult : "+e.getMessage());
                                Log.e(TAG,e.getStackTrace().toString());
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Log.i(TAG,"Settings unavailable");
                            break;
                    }
                }
            });
        }else{
            requestNearestKajian(false,null);
        }
    }
    private NearestKajianListingFragment getNearestKajianListingFragment(){
        return this;
    }
    private void requestNearestKajian(final boolean isLoadMore,String params){
        Log.d(TAG,"in requestnearestKajian");
        retrieveLastLocation();
        if(lastLocation != null){
            //api url
            String url = APIConnectionHelper.host + "/kajian/nearestKajianList" +
                    "/"+lastLocation.getLatitude()+"/"+lastLocation.getLongitude();

            if(params!=null){
                url+=params;
            }
            Log.d(TAG,"url : "+url);
            Log.d(TAG,"load more : false");

            StringRequest nearestKajianRequest = new StringRequest(
                    Request.Method.GET,
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonOResponse = new JSONObject(response.toString());
                                if (jsonOResponse.getBoolean("status")) {
                                    if (!jsonOResponse.isNull("collection")) {
                                        if(isLoadMore){
                                            nearestKajianPresenter.loadMoreNearestKajian(response.toString());
                                        }else{
                                            nearestKajianPresenter.loadNearestKajian(response.toString());
                                            kajianElementAdapter = new NearestKajianItemAdapter(getNearestKajianListingFragment(), nearestKajianList);
                                            setListAdapter(kajianElementAdapter);
                                        }
                                        closeDlgProgLoadNearestKajian();

                                    } else {
                                        removeFooterViewLoading();
                                        Log.i(TAG, "collection is null");
                                    }
                                } else {
                                    displayErrorDialog("Error while loading data");
                                    Log.e(TAG, jsonOResponse.getString("message"));
                                }

                            } catch (JSONException | ParseException e) {
                                e.printStackTrace();
                                Log.e(TAG, "Error while retrieving json nearest kajian : " + e.getMessage());
                                displayErrorDialog("Error while loading data");
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(TAG, "Error on request data (onErrorResponse method) : " + error.getMessage());
                            error.printStackTrace();
                            displayErrorDialog("Error while loading data");
                        }
                    }
            );
            APIConnectionHelper.getInstance(getContext()).addToRequestQueue(nearestKajianRequest);
        }else{
            Log.e(TAG,"last location is null");
            displayErrorDialog(getString(R.string.error_message_check_internet_connection));
        }
    }

    private void closeDlgProgLoadNearestKajian(){
        //close prgress dialog
        if(dlgProgLoadNearestKajian != null)
            if(dlgProgLoadNearestKajian.isShowing())
                dlgProgLoadNearestKajian.dismiss();
    }
    //endregion

    //region implemented methods from ConnectionCallbacks
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        processRequestingNearestKajian();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
    //endregion

    //region implemented methods from OnConnectionFailedListener
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        displayErrorDialog(getString(R.string.error_message_loading_data));
        Log.e(TAG,"connection faliled : "+connectionResult.getErrorMessage());
    }
    //endregion

    private class LoadMoreKajianTask extends AsyncTask<Void, Integer, Void>{
        @Override
        protected Void doInBackground(Void... params) {
            List<Kajian> nearestKajianList = kajianElementAdapter.getNearestKajianList();
            Kajian lastITem = nearestKajianList.get(nearestKajianList.size()-1);
            int idKajian = lastITem.getIdKajian();
            int idMosque = lastITem.getMosque().getIdMosque();
            Log.d(TAG,"id kajian : "+idKajian+"; id mosque : "+idMosque);
            requestNearestKajian(true,"/"+idKajian+"/"+idMosque);
            return null;
        }

        @Override
        protected void onPreExecute() {
            vFooterLoading = ((LayoutInflater)getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.layout_footer_loading,null,false);

            if(getListView().getFooterViewsCount() == 0){
                getListView().addFooterView(vFooterLoading);
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            removeFooterViewLoading();
        }
    }
}
