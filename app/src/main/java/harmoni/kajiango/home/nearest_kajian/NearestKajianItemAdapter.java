package harmoni.kajiango.home.nearest_kajian;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;

import harmoni.kajiango.R;
import harmoni.kajiango.common_activity.FullScreenImageActivity;
import harmoni.kajiango.detail_kajian.DetailKajianActivity;
import harmoni.kajiango.engine.helper.APIConnectionHelper;
import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 02-Dec-16.
 */
public class NearestKajianItemAdapter extends BaseAdapter {
    //region attributes
    private final String TAG = "NearestKajianItemAdp";
    private Activity activity;
    private List<Kajian> nearestKajianList;
    private static LayoutInflater layoutInflater = null;
    NearestKajianListingFragment nearestKajianListingFragment;
    private View view;
    //endregion

    //region constructors
    public NearestKajianItemAdapter(NearestKajianListingFragment nearestKajianListingFragment, List<Kajian> nearestKajianList) {
        this.activity = nearestKajianListingFragment.getActivity();
        this.nearestKajianListingFragment = nearestKajianListingFragment;
        this.nearestKajianList = nearestKajianList;
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    //endregion

    //region overridden methods
    @Override
    public int getCount() {
        return nearestKajianList.size();
    }

    @Override
    public java.lang.Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        view = convertView;
        if(convertView == null) view = layoutInflater.inflate(R.layout.frghome_kajian_list_item,null);
        //START init view
        TextView txtTitle = (TextView) view.findViewById(R.id.TxtKajianTitle);
        TextView txtDate = (TextView) view.findViewById(R.id.TxtDate);
        TextView txtTime = (TextView) view.findViewById(R.id.TxtTime);
        TextView txtAddress = (TextView) view.findViewById(R.id.TxtAddress);
        TextView txtMosque = (TextView) view.findViewById(R.id.TxtMosque);
        TextView txtSpeaker = (TextView) view.findViewById(R.id.TxtSpeaker);
        TextView txtAttend = (TextView) view.findViewById(R.id.TxtAttend);
        Button btnDetail = (Button) view.findViewById(R.id.BtnDetail);
        final ImageView imgPosterThumb = (ImageView) view.findViewById(R.id.ImgPosterThumb);
        //END init view

         final Kajian kajian = nearestKajianList.get(position);

        //START set titile
        if(kajian.getName().length() > 100){
            txtTitle.setText(kajian.getName().substring(0,100)+"...");
        }else{
            txtTitle.setText(kajian.getName());
        }

        //END set title

        //START set date
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        String stringDate = view.getResources().getString(R.string.kajian_listing_date,
                dateFormat.format(kajian.getDate()));
        txtDate.setText(stringDate);
        //END set date

        //START set time
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        String stringTime = view.getResources().getString(R.string.kajian_listing_time,
                timeFormat.format(kajian.getTimeStart().getTime()),
                timeFormat.format(kajian.getTimeEnd().getTime()));
        txtTime.setText(stringTime);
        //END set time

        //START set address
        //TODO limit text
        String mosqueAddress = kajian.getMosque().getAddress();
        if(mosqueAddress.length() > 110) mosqueAddress=mosqueAddress.substring(0,110)+" ...";
        txtAddress.setText(mosqueAddress);
        //END set address

        //START set mosque
        //TODO limit text
        String mosqueName = kajian.getMosque().getName().trim();
        if(!"masjid".equalsIgnoreCase(mosqueName.split("\\s+")[0])){
            mosqueName ="Masjid "+mosqueName;
        }
        txtMosque.setText(mosqueName+", "+kajian.getPlace());
        //END set mosque

        //START set speaker
        String stringSpeaker = view.getResources().getString(R.string.kajian_listing_speaker,
                kajian.getUstadz().getName());
        txtSpeaker.setText(stringSpeaker);
        //END set speaker

        //START set attend
        String stringAttend = view.getResources().getString(R.string.kajian_listing_attend,
                kajian.getAttendance());
        txtAttend.setText(stringAttend);
        //END set attend

        //START set image
        Picasso.with(activity)
                .load(APIConnectionHelper.host+kajian.getPic())
                .placeholder(R.drawable.img_apple_550_900)
                .error(R.drawable.img_apple_550_900)
                .into(imgPosterThumb);
        //END set image

        //START est button detail
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kajianDetailIntent = new Intent(view.getContext(), DetailKajianActivity.class);
                kajianDetailIntent.putExtra(DetailKajianActivity.KAJIAN_ID_EXTRA,kajian.getIdKajian());
                kajianDetailIntent.putExtra(DetailKajianActivity.MOSQUE_ID_EXTRA,kajian.getMosque().getIdMosque());
                view.getContext().startActivity(kajianDetailIntent);
            }
        });
        //END set button detail

        imgPosterThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*START show dialog fullscreen image*/
                Intent imageFullScreenIntent = new Intent(activity.getApplicationContext(), FullScreenImageActivity.class);
                imageFullScreenIntent.putExtra(FullScreenImageActivity.EXTRA_IMAGE,
                        APIConnectionHelper.host+kajian.getPic());
                activity.startActivity(imageFullScreenIntent);
                /*END show dialog fullscreen image*/
            }
        });

        view.setTag(R.id.ID_KAJIAN,kajian.getIdKajian());
        view.setTag(R.id.ID_MOSQUE,kajian.getMosque().getIdMosque());
        return view;
    }
    //endregion

    //region public methods
    public void updateData(List<Kajian> otherNearestKajianList){
        this.nearestKajianList.addAll(otherNearestKajianList);
        this.notifyDataSetChanged();
    }
    //endregion

    //region getter setter
    public List<Kajian> getNearestKajianList(){return this.nearestKajianList;}
    //endregion
}
