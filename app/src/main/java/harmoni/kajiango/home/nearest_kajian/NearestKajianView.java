package harmoni.kajiango.home.nearest_kajian;

import java.util.List;

import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public interface NearestKajianView {
    void showNearestKajian(List<Kajian> nearestKajianList);
    void showMoreNearestKajian(List<Kajian> nearestKajianList);
}
