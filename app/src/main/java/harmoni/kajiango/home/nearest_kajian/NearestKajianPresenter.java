package harmoni.kajiango.home.nearest_kajian;

import org.json.JSONException;

import java.text.ParseException;
import java.util.List;

import harmoni.kajiango.engine.APIConnection.HomeService.NearestKajianService;
import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 01-Jan-17.
 */
public class NearestKajianPresenter {
    //region attributes
    private NearestKajianView nearestKajianView;
    private NearestKajianService nearestKajianService;
    //endregion

    //region constructors
    public NearestKajianPresenter(NearestKajianView nearestKajianView){
        this.nearestKajianView = nearestKajianView;
        nearestKajianService = new NearestKajianService();
    }
    //endregion

    //region public methdos
    public void loadNearestKajian(String jsonNearestKajian) throws JSONException, ParseException {
        List<Kajian> nearestKajianList = nearestKajianService.loadNearestKajianList(jsonNearestKajian);
        nearestKajianView.showNearestKajian(nearestKajianList);
    }

    /**
     * Add more item when scrolling to last item in nearest kajian listing
     */
    public void loadMoreNearestKajian(String jsonNearestKajian) throws JSONException, ParseException {
        List<Kajian> nearestKajianList = nearestKajianService.loadNearestKajianList(jsonNearestKajian);
        nearestKajianView.showMoreNearestKajian(nearestKajianList);
    }
    //endregion
}
