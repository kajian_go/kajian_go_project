package harmoni.kajiango.detail_mosque;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import harmoni.kajiango.R;
import harmoni.kajiango.common_activity.FullScreenImageActivity;

public class DetailMosqueActivity extends AppCompatActivity {

    //region attributes
    private String mosqueName="Mosque Name";
    private ImageView imgMosque;
    //endregion

    //region overriden methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_mosque);

        initView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    //endregion

    //region public methods
    public void onClick(View view){
        switch (view.getId()){
            case R.id.ImgMosque:
                /*START show dialog fullscreen image*/
                Intent imageFullScreenIntent = new Intent(getApplicationContext(), FullScreenImageActivity.class);
                imageFullScreenIntent.putExtra(FullScreenImageActivity.EXTRA_IMAGE,R.drawable.img_food_480_800);
                startActivity(imageFullScreenIntent);
                /*END show dialog fullscreen image*/
                break;
        }
    }
    //endregion

    //region private methods
    private void initView(){
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(mosqueName);
        }
    }
    //endregion
}
