package harmoni.kajiango.donasi;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import harmoni.kajiango.R;
import harmoni.kajiango.engine.helper.APIConnectionHelper;
import harmoni.kajiango.model.DonationBankAccount;
import harmoni.kajiango.utils.AppConstants;

public class DonationActivity extends AppCompatActivity {
    private TextView txtNotes;
    private ImageView imgUpload;
    private EditText edtEmail;
    private EditText edtName;
    private static final int PICK_IMAGE_RESULT = 1;
    private static final int CAPTURE_IMAGE_RESULT = 2;
    private final String TAG = "DonationActivity";
    private ProgressDialog prgDialogUploadingData;
    private Uri capturedPhotoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation);

        initView();

        txtNotes.setText(Html.fromHtml(getString(R.string.donation_note)));

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);//enable back button on title bar
            actionBar.setTitle(getString(R.string.module_donation)); //set title
        }else{
            Log.e("TAG","action bar is null");
            displayErrorDialog(getString(R.string.error_activity));
        }

        requestDonationBankAccountList();
    }

    //region Control Event
    public void onClick(View view){
        switch(view.getId()){
            case R.id.BtnUpload :
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, PICK_IMAGE_RESULT );
            break;

            case R.id.BtnTakePhoto :
                Intent capture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if(capture.resolveActivity(getPackageManager())!=null){
                    File capturedImageFile = null;
                    try {
                        capturedImageFile = createImageFile();
                    } catch (IOException e) {
                        Log.e(TAG,"Error occured while creating image file "+e.getMessage());
                        displayErrorDialog(getString(R.string.donation_error_message_create_image));
                        e.printStackTrace();
                    }

                    if(capturedImageFile!=null){
                        capturedPhotoUri = Uri.fromFile(capturedImageFile);
                        capture.putExtra(MediaStore.EXTRA_OUTPUT, capturedPhotoUri);
                        startActivityForResult(capture,CAPTURE_IMAGE_RESULT);
                    }
                }

                break;

            case R.id.BtnSubmit :
                submitForm();
                break;
        }
    }

    private void submitForm() {
        prgDialogUploadingData = new ProgressDialog(this);
        prgDialogUploadingData.setMessage(getString(R.string.info_message_sending_data));
        prgDialogUploadingData.setCanceledOnTouchOutside(false);
        prgDialogUploadingData.setIndeterminate(true);
        prgDialogUploadingData.show();

        final String donorName = (edtName.getText() == null)? "" : edtName.getText().toString();;
        final String donorEmail = (edtEmail.getText() == null)? "" : edtEmail.getText().toString();

        String urlRequest = APIConnectionHelper.host+"/Donation/insertDonor/";
        StringRequest submitFormrequest = new StringRequest(Request.Method.POST, urlRequest,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonOResponse = new JSONObject(response);
                            if (!jsonOResponse.isNull(AppConstants.JSONIDonationBankAccount.STATUS)) {
                                if(jsonOResponse.getBoolean(AppConstants.JSONIDonationBankAccount.STATUS)){
                                    displaySuccessDialog(getString(R.string.donation_info_message_datasent));
                                    edtName.setText("");
                                    edtEmail.setText("");
                                    imgUpload.setImageResource(R.drawable.img_ic_image);
                                }else{
                                    throw new Exception("Failed to store data");
                                }

                            } else {
                                throw new Exception(response);
                            }
                        } catch (JSONException e) {
                            displayErrorDialog(getString(R.string.donation_error_message_datanotsent));
                            Log.e(TAG,e.getMessage());
                            e.printStackTrace();
                        } catch (Exception e) {
                            displayErrorDialog(getString(R.string.donation_error_message_datanotsent));
                            Log.e(TAG,e.getMessage());
                            e.printStackTrace();
                        }finally {
                            if((prgDialogUploadingData != null) && prgDialogUploadingData.isShowing()){
                                prgDialogUploadingData.dismiss();
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        displayErrorDialog(getString(R.string.donation_error_message_datanotsent));
                        Log.e(TAG,error.getMessage());
                        error.printStackTrace();
                        if((prgDialogUploadingData != null) && prgDialogUploadingData.isShowing()){
                            prgDialogUploadingData.dismiss();
                        }
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();

                params.put("name",donorName);
                params.put("email",donorEmail);

                String strBase64Image;

                if((imgUpload.getDrawable() != null) &&
                        (imgUpload.getDrawable() != ContextCompat.getDrawable(getApplicationContext(), R.drawable.img_ic_image))){
                    Bitmap imgReceipt = ((BitmapDrawable)imgUpload.getDrawable()).getBitmap();
                    strBase64Image = convertBitmapToString(imgReceipt);
                    params.put("image",strBase64Image);
                    imgReceipt = null;
                    strBase64Image=null;
                }
                return params;
            }
        };
        APIConnectionHelper.getInstance(this).addToRequestQueue(submitFormrequest);
    }

    private String convertBitmapToString(Bitmap imgBitmap){
        String imgString;
        ByteArrayOutputStream imgStream = new ByteArrayOutputStream();
        imgBitmap.compress(Bitmap.CompressFormat.JPEG,100,imgStream);
        byte[] arrImgStream = imgStream.toByteArray();
        imgString = Base64.encodeToString(arrImgStream,Base64.DEFAULT);
        return imgString;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "kajian_go_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    //endregion


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK ){
            switch (requestCode){
                case PICK_IMAGE_RESULT :
                    if(data != null){
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};

                        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        if (cursor != null) {
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            String decodableString = cursor.getString(columnIndex);
                            cursor.close();

                            imgUpload.setImageBitmap(BitmapFactory.decodeFile(decodableString));
                        }else{
                            Toast.makeText(getApplicationContext(), getString(R.string.txt_err_pick_image), Toast.LENGTH_LONG).show();
                        }
                    }
                break;

                case CAPTURE_IMAGE_RESULT :
                    ContentResolver contentResolver = this.getContentResolver();
                    Bitmap bitmap;
                    try
                    {
                        Log.e(TAG,"getting bitmap from mediastore");
                        bitmap = android.provider.MediaStore.Images.Media.getBitmap(contentResolver, capturedPhotoUri);
                        Log.e(TAG,"set image bitmap");
                        imgUpload.setImageBitmap(bitmap);
                        Log.e(TAG,"done");
                    }
                    catch (Exception e)
                    {
                        Log.e(TAG, "Failed to load image ", e);
                        displayErrorDialog(getString(R.string.error_message_load_image));
                    }
//                    Bundle extras = data.getExtras();
//                    Bitmap image = (Bitmap) extras.get("data");
//                    imgUpload.setImageBitmap(image);
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    private void initView(){
        txtNotes = (TextView) findViewById(R.id.TxtNotes);
        imgUpload = (ImageView) findViewById(R.id.ImgUpload);
        edtName = (EditText) findViewById(R.id.EdtName);
        edtEmail = (EditText) findViewById(R.id.EdtEmail);
    }

    private void requestDonationBankAccountList(){
        StringRequest requestDonatoinBankAccountList = new StringRequest(Request.Method.GET,
                APIConnectionHelper.host + "/donation/accountList",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            List<DonationBankAccount> donatonBankAccountList=parseJsonToDonationBankAccountList(response);
                            createBankAccountListView(donatonBankAccountList);
                        } catch (JSONException e) {
                            displayErrorDialog("Error while loading data");
                            Log.e(TAG,"JSONException : "+e.getMessage());
                            Log.e(TAG,e.getStackTrace().toString());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        displayErrorDialog("Error while loading data");
                        Log.e(TAG,"Response.ErrorListener : "+error.getMessage());
                        Log.e(TAG,error.getStackTrace().toString());
                    }
                });
        APIConnectionHelper.getInstance(this).addToRequestQueue(requestDonatoinBankAccountList);
    }

    private void createBankAccountListView(List<DonationBankAccount> donationBankAccountList){
        LinearLayout linBankAccount = (LinearLayout) findViewById(R.id.LinBankAccountList);
        LayoutInflater inflater = LayoutInflater.from(this);
        for(DonationBankAccount donationBankAccount : donationBankAccountList){
            View bankAccountAdapter = inflater.inflate(R.layout.adapter_bank_account,linBankAccount,false);
            //START init view
            TextView txtBankName = (TextView) bankAccountAdapter.findViewById(R.id.TxtBankName);
            TextView txtAccountNumber = (TextView) bankAccountAdapter.findViewById(R.id.TxtAccountNumber);
            TextView txtOwnerName = (TextView) bankAccountAdapter.findViewById(R.id.TxtOwnerName);
            ImageView imgBankLogo = (ImageView) bankAccountAdapter.findViewById(R.id.ImgBankLogo);
            //END init view

            //START set data
            txtBankName.setText(donationBankAccount.getBank());
            txtAccountNumber.setText(bankAccountAdapter.getResources().getString(R.string.donation_lbl_account_number)
                    +"\n"+donationBankAccount.getAccountNumber());
            txtOwnerName.setText(bankAccountAdapter.getResources().getString(R.string.donation_lbl_on_behalf)
                    +"\n"+donationBankAccount.getOwnerName());
            //END set data
            linBankAccount.addView(bankAccountAdapter);
            String imageUrl="";
            if(donationBankAccount.getImageUrl() != null || !donationBankAccount.getImageUrl().isEmpty()){
                if(donationBankAccount.getImageUrl().startsWith("http") || donationBankAccount.getImageUrl().startsWith("https")){
                    imageUrl = donationBankAccount.getImageUrl();
                }else{
                    imageUrl = APIConnectionHelper.host+"/"+donationBankAccount.getImageUrl();
                }
            }
            Picasso.Builder imgBankLogoLoader = new Picasso.Builder(this);
            imgBankLogoLoader.build()
                    .with(this)
                    .load(imageUrl)
                    .placeholder(R.drawable.img_200_200)
                    .error(R.drawable.img_apple_550_900)
                    .into(imgBankLogo);
        }
    }

    private List<DonationBankAccount> parseJsonToDonationBankAccountList(String jsonDonation) throws JSONException {
        List<DonationBankAccount> donationBankAccountList = new ArrayList<>();
        JSONObject jsonOCollection = new JSONObject(jsonDonation);
        JSONArray jsonABankAccount = jsonOCollection
                .getJSONArray(AppConstants.JSONIDonationBankAccount.COLLECTION);

        for (int i=0;i<jsonABankAccount.length();i++){
            JSONObject jsonOBankAccount = jsonABankAccount.getJSONObject(i);
            DonationBankAccount donationBankAccount = new DonationBankAccount();
            donationBankAccount.setAccountNumber(jsonOBankAccount
                    .getString(AppConstants.JSONIDonationBankAccount.ACCOUNT_NUMBER));
            donationBankAccount.setBank(jsonOBankAccount
                    .getString(AppConstants.JSONIDonationBankAccount.BANK));
            donationBankAccount.setOwnerName(jsonOBankAccount
                    .getString(AppConstants.JSONIDonationBankAccount.OWNER_NAME));
            donationBankAccount.setImageUrl(jsonOBankAccount
                    .getString(AppConstants.JSONIDonationBankAccount.IMAGE_URL));

            donationBankAccountList.add(donationBankAccount);
        }

        return donationBankAccountList;
    }

    private void displayErrorDialog(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.error_title));
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(getString(R.string.btn_ok), null);
        alertDialogBuilder.show();
    }

    private void displaySuccessDialog(String message){
        AlertDialog.Builder successDialog = new AlertDialog.Builder(this);
        successDialog.setMessage(message);
        successDialog.setPositiveButton(getString(R.string.btn_ok), null);
        successDialog.show();
    }
}
