package harmoni.kajiango.detail_kajian;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

import harmoni.kajiango.engine.APIConnection.DetailKajianService;
import harmoni.kajiango.model.Kajian;
import harmoni.kajiango.utils.AppConstants;

/**
 * Created by Aprilio Pajri on 21-Mar-17.
 */
public class DetailKajianPresenter {
    //region attributes
    private DetailKajianView detailKajianView;
    private DetailKajianService detailKajianService;
    //endregion

    //region constructors
    public DetailKajianPresenter(DetailKajianView detailKajianView) {
        this.detailKajianView = detailKajianView;
        detailKajianService = new DetailKajianService();
    }
    //endregion

    //region public methods
    public void loadDetailKajian(String response) throws JSONException, ParseException {
        Kajian detailKajian = detailKajianService.loadDetailKajian(response);
        detailKajianView.showDetailKajian(detailKajian);
    }

    public void updateStatus(String response) throws JSONException {
        JSONObject jsonOUpdateStatus = new JSONObject(response);

        //extract status
        boolean updateStatus =  Boolean.parseBoolean(jsonOUpdateStatus.getString(AppConstants.JSONIKajianDetail.STATUS));
        detailKajianView.respondStatus(updateStatus);

        //extract attend
        int attend = jsonOUpdateStatus.getInt(AppConstants.JSONIKajianDetail.ATTEND);
        detailKajianView.updateAttend(attend);
    }

    //endregion
}
