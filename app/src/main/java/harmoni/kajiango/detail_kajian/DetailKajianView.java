package harmoni.kajiango.detail_kajian;

import harmoni.kajiango.model.Kajian;

/**
 * Created by Aprilio Pajri on 21-Mar-17.
 */
public interface DetailKajianView {
    void showDetailKajian(Kajian detailKajian);
    void respondStatus(boolean status);
    void updateAttend(int attend);
}
