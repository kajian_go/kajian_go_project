package harmoni.kajiango.detail_kajian;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import harmoni.kajiango.R;
import harmoni.kajiango.common_activity.FullScreenImageActivity;
import harmoni.kajiango.common_activity.MapsActivity;
import harmoni.kajiango.engine.helper.APIConnectionHelper;
import harmoni.kajiango.model.Kajian;

public class DetailKajianActivity extends AppCompatActivity implements DetailKajianView{

    //region object view
    private TextView txtKajianTitle;
    private ImageView imgPosterKajian;
    private TextView txtTime;
    private TextView txtDate;
    private TextView txtMosque;
    private TextView txtAddress;
    private TextView txtSpeaker;
    private TextView txtAttend;
    private Button btnAttend;
    private Button btnViewMap;
    private TextView txtDescription;
    //endregion

    //region attributes
    public static final String KAJIAN_ID_EXTRA = "KAJIAN_ID_EXTRA";
    public static final String MOSQUE_ID_EXTRA = "MOSQUE_ID_EXTRA";
    private final String TAG = "DetailKajianActivity";
    private final String ATTEND_KEY_PREFIX = "attend";
    private int idKajian, idMosque;
    private DetailKajianPresenter detailKajianPresenter;
    private ActionBar actionBar;
    private SharedPreferences sharedPreferences;
    private ProgressDialog progDlgLoadDetailKajian;
    private boolean isUserAttend;
    //endregion

    //region overridden methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kajian);
        Intent intent = getIntent();
        idKajian = intent.getIntExtra(KAJIAN_ID_EXTRA,Integer.MIN_VALUE);
        idMosque = intent.getIntExtra(MOSQUE_ID_EXTRA,Integer.MIN_VALUE);

        if((idKajian != Integer.MIN_VALUE)
                &&(idMosque != Integer.MIN_VALUE)){
            showProgressDialogLoadingData();
            initKajian();
            initView();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    //endregion

    //region Public Methods
    public void onClick(View view){
        switch(view.getId()){
            case R.id.ImgPosterKajian :
                /*START show dialog fullscreen image*/
                Intent imageFullScreenIntent = new Intent(getApplicationContext(), FullScreenImageActivity.class);
                imageFullScreenIntent.putExtra(FullScreenImageActivity.EXTRA_IMAGE,R.drawable.img_food_480_800);
                startActivity(imageFullScreenIntent);
                /*END show dialog fullscreen image*/
                break;

            case R.id.BtnAttend :
                //TODO set text "Will Attend" with new value
                showProgressDialogLoadingData();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                if(btnAttend.getTag() == "attend"){
                    isUserAttend=true;
                    editor.putBoolean(ATTEND_KEY_PREFIX+idKajian+idMosque,true);
                    setBtnAttendMode(false);
                }else{
                    isUserAttend=false;
                    editor.putBoolean(ATTEND_KEY_PREFIX+idKajian+idMosque,false);
                    setBtnAttendMode(true);
                }
                editor.apply();

                StringRequest requestKajian = new StringRequest(Request.Method.GET,
                        APIConnectionHelper.host + "/kajian/updateKajianAttendance/" + idMosque
                                + "/" + idKajian+"/"+isUserAttend,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    detailKajianPresenter.updateStatus(response);
                                } catch (JSONException e) {
                                    displayErrorDialog("Error while loading data");
                                    Log.e(TAG,"JSONException : "+e.getMessage());
                                    Log.e(TAG,e.getStackTrace().toString());
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                displayErrorDialog("Error while loading data");
                                Log.e(TAG,"Response.ErrorListener : "+error.getMessage());
                                Log.e(TAG,error.getStackTrace().toString());
                            }
                        });
                APIConnectionHelper.getInstance(this).addToRequestQueue(requestKajian);
                break;

            default:
                Log.e(TAG,"Clickable view not handled");
                break;
        }
    }
    //endregion

    //region Private Methods
    private void showProgressDialogLoadingData(){
        //create progress dialog
        progDlgLoadDetailKajian = new ProgressDialog(this);
        progDlgLoadDetailKajian.setMessage(getString(R.string.info_message_loading_data));
        progDlgLoadDetailKajian.setIndeterminate(true);
        progDlgLoadDetailKajian.setCanceledOnTouchOutside(false);
        progDlgLoadDetailKajian.show(); //show progress dialog during requesting data
    }
    private void closeProgDlgLoadDetailKajian(){
        if(progDlgLoadDetailKajian != null)
            if(progDlgLoadDetailKajian.isShowing())
                progDlgLoadDetailKajian.dismiss();
    }
    private void displayErrorDialog(String message){
        closeProgDlgLoadDetailKajian();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Error");
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("OK",null);
        alertDialogBuilder.show();
    }

    private void initKajian() {
        detailKajianPresenter = new DetailKajianPresenter(this);
        StringRequest requestKajian = new StringRequest(Request.Method.GET,
                APIConnectionHelper.host + "/kajian/getKajianById/" + idMosque + "/" + idKajian,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            detailKajianPresenter.loadDetailKajian(response);
                        } catch (JSONException e) {
                            displayErrorDialog("Error while loading data");
                            Log.e(TAG,"JSONException : "+e.getMessage());
                            Log.e(TAG,e.getStackTrace().toString());
                            e.printStackTrace();
                        } catch (ParseException e) {
                            displayErrorDialog("Error while loading data");
                            Log.e(TAG,"ParseException : "+e.getMessage());
                            Log.e(TAG,e.getStackTrace().toString());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        displayErrorDialog("Error while loading data");
                        Log.e(TAG,"Response.ErrorListener : "+error.getMessage());
                        Log.e(TAG,error.getStackTrace().toString());
                    }
                });
        APIConnectionHelper.getInstance(this).addToRequestQueue(requestKajian);
    }

    private void initView(){
        actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("");
        }
        imgPosterKajian = (ImageView) findViewById(R.id.ImgPosterKajian);
        txtKajianTitle = (TextView) findViewById(R.id.TxtKajianTitle);
        txtTime = (TextView) findViewById(R.id.TxtTime);
        txtDate = (TextView) findViewById(R.id.TxtDate);
        txtMosque = (TextView) findViewById(R.id.TxtMosque);
        txtAddress = (TextView) findViewById(R.id.TxtAddress);
        txtSpeaker = (TextView) findViewById(R.id.TxtSpeaker);
        txtAttend = (TextView) findViewById(R.id.TxtAttend);
        btnAttend = (Button) findViewById(R.id.BtnAttend);
        btnViewMap = (Button) findViewById(R.id.BtnViewMap);
        txtDescription = (TextView) findViewById(R.id.TxtDescription);

        //check user has attend or not
        sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        boolean isAttended = sharedPreferences.getBoolean(ATTEND_KEY_PREFIX+idKajian+idMosque,false);
        btnAttend.setTag("not attend");
        if(isAttended){
            setBtnAttendMode(false);
        }else{
            setBtnAttendMode(true);
        }

    }

    private void setBtnAttendMode(boolean isAttend){
        btnAttend.setTag((isAttend) ? "attend" : "not attend");
        btnAttend.setBackgroundColor(ContextCompat.getColor(getApplicationContext()
                ,(isAttend)? R.color.colorBtnAttend : R.color.colorBtnUnAttend));
        btnAttend.setText(getString(isAttend ? R.string.kajian_detail_btn_attend
                : R.string.kajian_detail_btn_notattend));
    }
    //endregion

    //region implemented methods from DetailKajianView
    @Override
    public void showDetailKajian(final Kajian detailKajian) {
        txtKajianTitle.setText(detailKajian.getName());
        actionBar.setTitle(detailKajian.getName());

        Picasso.Builder imgPosterKajianLoader = new Picasso.Builder(this);
        imgPosterKajianLoader.listener(new Picasso.Listener() {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception e) {
                Log.e(TAG,"onImageLoadFailed : "+e.getMessage());
                Log.e(TAG,e.getStackTrace().toString());
                displayErrorDialog("Error while loading data");
            }
        });
        imgPosterKajianLoader.build()
                .with(this)
                .load(APIConnectionHelper.host+"/"+detailKajian.getPic())
                .placeholder(R.drawable.img_200_200)
                .error(R.drawable.img_apple_550_900)
                .into(imgPosterKajian, new Callback() {
                    @Override
                    public void onSuccess() {
                        imgPosterKajian.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent imgFullScreenintent = new Intent(getApplicationContext(),
                                        FullScreenImageActivity.class);
                                imgFullScreenintent.putExtra(FullScreenImageActivity.EXTRA_IMAGE,
                                        APIConnectionHelper.host+"/"+ detailKajian.getPic());

                                startActivity(imgFullScreenintent);
                            }
                        });
                    }

                    @Override
                    public void onError() {
                        Log.e(TAG, "Error while loading image file with picasso");
                        displayErrorDialog("Error while loading data");
                    }
                });

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        txtTime.setText(timeFormat.format(detailKajian.getTimeStart().getTime())+"-" +
                timeFormat.format(detailKajian.getTimeEnd().getTime())+" WIB");

        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMMM yyyy");
        txtDate.setText(dateFormatter.format(detailKajian.getDate()));
        txtMosque.setText(detailKajian.getMosque().getName()+", "+detailKajian.getPlace());
        txtAddress.setText(detailKajian.getMosque().getAddress());
        txtSpeaker.setText(detailKajian.getUstadz().getName());
        txtAttend.setText(String.valueOf(detailKajian.getAttendance()));

        //add action to button view map
        btnViewMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewMapIntent = new Intent(getApplicationContext(), MapsActivity.class);
                viewMapIntent.putExtra(MapsActivity.LAT_EXTRA, detailKajian.getMosque().getLatitude());
                viewMapIntent.putExtra(MapsActivity.LNG_EXTRA, detailKajian.getMosque().getLongitude());

                if(detailKajian.getName().toLowerCase().startsWith("kajian"))
                    viewMapIntent.putExtra(MapsActivity.TITLE_EXTRA,detailKajian.getName());
                else
                    viewMapIntent.putExtra(MapsActivity.TITLE_EXTRA,"Kajian "+detailKajian.getName());

                startActivity(viewMapIntent);
            }
        });

        txtDescription.setText(Html.fromHtml(detailKajian.getDescription()));

        closeProgDlgLoadDetailKajian();
    }

    @Override
    public void respondStatus(boolean status) {
        if(!status){
            displayErrorDialog(getString(R.string.error_message_loading_data));
        }else{
            AlertDialog.Builder dlgUserAttend = new AlertDialog.Builder(this);
            if(isUserAttend)
                dlgUserAttend.setMessage(getString(R.string.kajian_detail_msg_attend));
            else
                dlgUserAttend.setMessage(getString(R.string.kajian_detail_msg_not_attend));

            dlgUserAttend.setPositiveButton("OK",null);
            dlgUserAttend.show();
        }

        closeProgDlgLoadDetailKajian();
    }

    @Override
    public void updateAttend(int attend) {
        txtAttend.setText(String.valueOf(attend));
    }

    //endregion
}
